<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Contact;
class ContactController extends Controller
{
    //
    public $successStatus = 200;

	public function get(Request $request){
		$user=$request->user();

    	$contacts=$user->contacts;
    	$contactsResult = array();
    	foreach ($contacts as $contact)  {
    		
    		$object = (object)['id'=>$contact->id,'firstName'=>$contact->firstName,'lastName'=>$contact->lastName,'phone'=>$contact->phone,'email'=>$contact->email,'address'=>$contact->address,'service'=>$contact->service];
    		array_push($contactsResult,$object);  
    	}
    	return response()->json(['contacts' => $contactsResult], $this-> successStatus);
    }
    public function store(Request $request){
    	$validator=Validator::make($request->all(), [ 
            'email' => 'email',  
            'firstName'=>'required|string',
            'lastName'=>'required|string',
            'address'=>'string',
            'service'=> 'string',
            'phone'=> 'required|string',


        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 400);            
        }
        $contact=new Contact();
        $contact->firstName=$request->firstName;
        $contact->lastName=$request->lastName;
        $contact->firstName=$request->firstName;
        $contact->user_id=$request->user()->id;
        $contact->phone=$request->phone;
        if($request->address) $contact->address=$request->address;
        if($request->email) $contact->email=$request->email;
        if($request->service) $contact->service=$request->service;
        $contact->save();
        return response()->json(['success' => true], $this-> successStatus);

    }
    public function delete(Request $request){
      $validator=Validator::make($request->all(),[
        'id'=>'required|integer'
      ]);
      if($validator->fails()){
        return response()->json(['error'=>$validator->errors()], 400);
      }
      $contact =Contact::find($request->id);
      if($contact!=null){
      	if($contact->delete()){
      		 return response()->json(['deleted' => true], $this-> successStatus);
      	}
      	else{
      		return response()->json(['deleted'=>"suppression impossibele,une erreur s'est produit"], 501);
      	}
      }
      else{
      	return response()->json(['deleted'=>"le contact que vous essayer de supprimer n'existe pas"], 500);
      }
      
      
    }
    public function update(Request $request){
    	$validator=Validator::make($request->all(), [ 
            'email' => 'email',  
            'firstName'=>'string',
            'lastName'=>'string',
            'address'=>'string',
            'service'=> 'string',
            'phone'=> 'string',


        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 400);            
        }
        $contact =Contact::find($request->id);
        if($contact!=null){
      		if($request->address) $contact->address=$request->address;
        	if($request->email) $contact->email=$request->email;
        	if($request->service) $contact->service=$request->service;
        	if($request->firstName) $contact->firstName=$request->firstName;
        	if($request->lastName) $contact->lastName=$request->lastName;
        	if($request->phone) $contact->phone=$request->phone;
        	$contact->save();
        	return response()->json(['updated' => true], $this-> successStatus);
      	}
      	else{
      	return response()->json(['updated'=>"le contact que vous essayer de modifier n'existe pas"], 500);
      	}
        


    }

}
