<?php

namespace App\Http\Controllers;
use Validator;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth; 

class UserController extends Controller
{
    //
   public $successStatus = 200;
    public function login(Request $request){

    	$validator=Validator::make($request->all(), [ 
            'email' => 'required|email', 
            'password' => 'required|min:8', 
        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 401);            
        }
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
        	$user=Auth::user(); 
        	$success['token'] =  $user->createToken('welcome to Rachad Tp for mobile developpement app')-> accessToken; 
            

        	return response()->json(['success' => $success], $this-> successStatus); 
        }
        else{
        	return response()->json(['error'=>'Unauthorised'], 401);
        } 
     }
     public function signUp(Request $request){
     	$validator=Validator::make($request->all(), [ 
            'email' => 'required|email|unique:users', 
            'password' => 'required|min:8', 
            'firstName'=>'required|string',
            'lastName'=>'required|string',
            'address'=>'required|string',
            'service'=> 'required|string',
            'phone'=> 'required|string',


        ]);
        if ($validator->fails()) { 
            return response()->json(['error'=>$validator->errors()], 400);            
        }
        $user=new User(
        [
            'firstName' => $request->firstName,
            'lastName'=>$request->lastName,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'address' => $request->address,
            'service'=> $request->service,
            'phone'=>$request->phone

        ]);

        $user->save();
        $success['token'] =  $user->createToken('welcome to Rachad Tp for mobile developpement app')-> accessToken;  
        return response()->json(['success' => $success], $this-> successStatus);
        
     }
     public function infos(Request $request){
        $user = Auth::user(); 
        
        return response()->json(['firstName' => $user->firstName,"lastName"=>$user->lastName,"email"=>$user->email,"address"=>$user->address,"phone"=>$user->phone,"service"=>$user->service], $this-> successStatus); 
     }
     
}
