<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('login', 'UserController@login');
Route::post('signup', 'UserController@signUp');

Route::group(['middleware' => 'auth:api'], function(){
Route::post('infos', 'UserController@infos');
Route::get('contacts','ContactController@get');
Route::post('contacts','ContactController@store');
Route::delete('contacts','ContactController@delete');
Route::post('updatecontact','ContactController@update');
});