import { Component,forwardRef,Inject } from '@angular/core';
import { IonicPage,NavController,NavParams ,ToastController,AlertController} from 'ionic-angular';
import { ContactProvider } from '../../providers/contact/contact';
import {AuthProvider} from '../../providers/auth/auth';
import {LoginSignupPage} from '../../pages/login-signup/login-signup';
import {ContactInfosPage} from '../../pages/contact-infos/contact-infos';
import {TabsPage} from '../../pages/tabs/tabs';
import {Subscription } from 'rxjs/Subscription';
/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  public contacts=[];
  public searchInput="";
  contactSubscription:Subscription;
  constructor(public navCtrl: NavController, public navParams: NavParams,private contactProvider:ContactProvider,private toastController:ToastController,private alertController:AlertController,private authProvider:AuthProvider,@Inject(forwardRef(()=>TabsPage))private tabs:TabsPage) {
  		this.contactProvider.loadContacts();
      
  }

  ionViewDidLoad() {
  	this.contactSubscription=this.contactProvider.contactsSubject.subscribe(
    (contacts:any[])=>{
    	this.contacts=contacts;
    	
    }
    );
	this.contactProvider.loadContacts();
  }
  onAdd(){
  	let alert=this.alertController.create({
  	title:"Nouveau Contat",
  	message:"saisisez les infos du contact",
  	inputs:[
  		{
  		type:"text",
  		name:"firstName",
  		placeholder: 'nom'
  		},
  		{
  		type:"text",
  		name:"lastName"
  		,placeholder: 'prenom'
  		},
  		{
  		type:"text",
  		name:"phone",
  		placeholder: 'telephone'
  		},
  		{
  		type:"email",
  		name:"email",
  		placeholder: 'email'
  		},
  		{
  		type:"text",
  		name:"address",
  		placeholder: 'address'
  		},
  		{
  		type:"text",
  		name:"service",
  		placeholder: 'service'
  		}
  	],
  	buttons:[
  	{
  		text:"annuler"
  	},
  	{
  		text:"ajouter",
  		handler:(inputData)=>{
  		if(inputData.firstName.length==0||inputData.lastName.length==0||inputData.phone.length==0){
  				let toastMessage=this.toastController.create({
  				message:"les champs nom,prenom,telephone seont requis",
  					duration:5000
  				});
  				toastMessage.present();
  		}
  		else{
  			var contact={
  				firstName:inputData.firstName,
  				lastName:inputData.lastName,
  				phone:inputData.phone,
  				email:inputData.email,
  				address:inputData.address,
  				service:inputData.service

  			}
  			this.contactProvider.addContact(contact);
  		}
  		
  	
  		}

  	}
  	]
  	});
  	alert.present();
  }

  onDelete(id:number){
  	let alert=this.alertController.create({
  	title:"message de confirmation",
  	message:"supprimer le contact ?",
  	buttons:[
  	{
  		text:"annuler"
  	},
  	{
  		text:"confirmer",
  		handler:()=>{
  		this.contactProvider.deleteContact(id);
  	
  		}

  	}
  	]
  	});
  	alert.present();
  }
  onUpdate(id:number){
  	let alert=this.alertController.create({
  	title:"Modifier Contat",
  	message:"saisisez les infos du contact",
  	inputs:[
  		{
  		type:"text",
  		name:"firstName",
  		placeholder: 'nom'
  		},
  		{
  		type:"text",
  		name:"lastName"
  		,placeholder: 'prenom'
  		},
  		{
  		type:"text",
  		name:"phone",
  		placeholder: 'telephone'
  		},
  		{
  		type:"email",
  		name:"email",
  		placeholder: 'email'
  		},
  		{
  		type:"text",
  		name:"address",
  		placeholder: 'address'
  		},
  		{
  		type:"text",
  		name:"service",
  		placeholder: 'service'
  		}
  	],
  	buttons:[
  	{
  		text:"annuler"
  	},
  	{
  		text:"ajouter",
  		handler:(inputData)=>{
  		if(inputData.firstName.length==0 && inputData.lastName.length==0 && inputData.phone.length==0 && inputData.email.length==0 && inputData.address.length==0 && inputData.service.length==0){
  				let toastMessage=this.toastController.create({
  				message:"veuillez saisir au moins un champ !",
  					duration:5000
  				});
  				toastMessage.present();
  		}
  		else{
  			var contact={
  				firstName:inputData.firstName,
  				lastName:inputData.lastName,
  				phone:inputData.phone,
  				email:inputData.email,
  				address:inputData.address,
  				service:inputData.service

  			}
  			this.contactProvider.updateCategorie(contact,id);
  		}
  		
  	
  		}

  	}
  	]
  	});
  	alert.present();
  }
  onlogout(){
  		this.authProvider.logout();
  		this.tabs.switchToLoginPage();
  }
  onDetails(i){

      this.navCtrl.push(ContactInfosPage,{'contact':i});
      console.log(i);
  }
  search(){
  if(this.searchInput.length!=0){
     this.contacts=this.contactProvider.searchContacts(this.searchInput);
  }
  else{
  this.contactProvider.emitContactsSkills();
  }
 
  }
 
  }
  


