import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {AuthProvider} from '../../providers/auth/auth';
import {Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {
  user={};
  userSubscription:Subscription;
  constructor(public navCtrl: NavController,private authService:AuthProvider ) {
  	this.userSubscription=this.authService.userSubject.subscribe(
    (user:any)=>{
      
      this.user=user;
      
    }
    );
  this.authService.loadUserInfos();
  }

}
