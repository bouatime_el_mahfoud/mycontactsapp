import { Component ,forwardRef,Inject} from '@angular/core';
import {AuthProvider} from '../../providers/auth/auth';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import {LoginSignupPage} from '../../pages/login-signup/login-signup';
import { HomePage } from '../home/home';
import { NavController } from 'ionic-angular';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
 

  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = ContactPage;

  constructor(private nav:NavController) {
  

  }
  switchToLoginPage(){
  	this.nav.push(LoginSignupPage);
  }
}
