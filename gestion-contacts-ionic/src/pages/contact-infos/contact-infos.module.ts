import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ContactInfosPage } from './contact-infos';

@NgModule({
  declarations: [
    ContactInfosPage,
  ],
  imports: [
    IonicPageModule.forChild(ContactInfosPage),
  ],
})
export class ContactInfosPageModule {}
