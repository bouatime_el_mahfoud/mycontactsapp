import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import { FormGroup,FormBuilder,Validators} from '@angular/forms';
import {AuthProvider} from '../../providers/auth/auth';
import {TabsPage} from '../../pages/tabs/tabs';
import { HttpClient ,HttpHeaders} from '@angular/common/http';

/**
 * Generated class for the LoginSignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login-signup',
  templateUrl: 'login-signup.html',
})
export class LoginSignupPage {
  public isLogin=true;
  public signInForm:FormGroup;
  public signUpForm:FormGroup;
  constructor(public navCtrl: NavController, public navParams: NavParams,private formBuilder:FormBuilder,private authProvider:AuthProvider,private httpClient:HttpClient,private toastController:ToastController) {
  this.initForm();
  }

  ionViewDidLoad() {
    
  }
  initForm(){
  	if(this.isLogin){
  		this.signInForm=this.formBuilder.group(
  			{
  		email:['',[Validators.required,Validators.email]]
  		,password:['',[Validators.required,Validators.minLength(8)]]
  		}
  			);
  	}
  	else{
  		this.signUpForm=this.formBuilder.group(
  			{
  		   firstName:['',Validators.required],
  		   lastName:['',Validators.required],
  		   phone:['',[Validators.required,Validators.minLength(10)]],
  		   email:['',[Validators.required,Validators.email]],
  		  password:['',[Validators.required,Validators.minLength(8)]],
  		  address:['',Validators.required],
  		  service:['',Validators.required]

  		}
  			);
  	}
  }
  onSwitch(){
  	this.isLogin=!this.isLogin;
  	this.initForm();
  }
   onSubmitForm(){
  	if(this.isLogin){
 		this.login(
    	this.signInForm.value['email'],this.signInForm.value['password']
   		 );
   		 
   		 

 	}
 	else{
 		this.signUp(this.signUpForm.value);
 			
 	}
 	
  }
  login(email:string,password:string){
	
	this.httpClient.post('http://localhost:8000/api/login',{email:email,password:password}).subscribe(
	(res:any)=>{
		this.authProvider.setToken('Bearer '+res.success.token);
		let toastMessage=this.toastController.create({
  			message:"connexio reussie",
  			duration:2000
  		});
  		toastMessage.present();
  		this.navCtrl.push(TabsPage);
		
	},
	err=>{
		if(err.status==401){
			let toastMessage=this.toastController.create({
  			message:"email ou mot de passe incorect",
  			duration:2000
  		});
  		toastMessage.present();
			
		}
		else {
		
		let toastMessage=this.toastController.create({
  		message:"une erreur s'est produit ,veuillez essayer ulterieruement",
  		duration:2000
  		});
  		toastMessage.present();
			
		}
		return false;
	}

	);
	}
	signUp(user:any){
		
		this.httpClient.post('http://localhost:8000/api/signup',{email:user.email,password:user.password,firstName:user.firstName,lastName:user.lastName,phone:user.phone,address:user.address,service:user.service}).subscribe(
			(res:any)=>{
				this.authProvider.setToken('Bearer '+res.success.token);
				let toastMessage=this.toastController.create({
  				message:"inscription valid",
  				duration:2000
  				});
  				toastMessage.present();

  				this.onSwitch();
  				
			},	
			err=>{
			if(err.status==400){
			let toastMessage=this.toastController.create({
  				message:"cette address email est déja utilisé",
  				duration:2000
  				});
  				toastMessage.present();
			
			}
			else {
				let toastMessage=this.toastController.create({
  				message:"une erreur s'est produit veuillez ressayer ulterieurement",
  				duration:2000
  				});
  				toastMessage.present();	
			}
			
			}
		);
	}
 

}
