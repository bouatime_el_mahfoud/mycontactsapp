import { HttpClient ,HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { Subject } from 'rxjs/Subject';


/*
  Generated class for the AuthProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthProvider {
  storageKey:string="token";
  userSubject=new Subject<any>();
  private user:any;
  constructor(private httpClient: HttpClient,private toastController:ToastController) {
    console.log('Hello AuthProvider Provider');
  }

  setToken(token:string){
		localStorage.setItem(this.storageKey,token);
	}

	getToken(){
		return localStorage.getItem(this.storageKey);
	}
	
	logout(){
		localStorage.removeItem(this.storageKey);
		



	}
	emitUserSubject(){
		this.userSubject.next(this.user);
	}
	
	
loadUserInfos(){
		let headers = new HttpHeaders({'Accept':'application/json','Authorization':this.getToken()});
		this.httpClient.post('http://localhost:8000/api/infos',{},{headers:headers}).subscribe(
			(res:any)=>{
			
			this.user=res;
			this.emitUserSubject();
			},
			err=>{
				console.log(err);
			}
		);
	}

}
