import { HttpClient ,HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';
import { Subject } from 'rxjs/Subject';
import {AuthProvider} from '../auth/auth';
/*
  Generated class for the ContactProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ContactProvider {
  private contacts=[];
  contactsSubject=new Subject<any[]>();

emitContactsSkills(){
		this.contactsSubject.next(this.contacts.slice());
}
  constructor(public httpClient: HttpClient,private toastController:ToastController,private authService:AuthProvider) {
    console.log('Hello ContactProvider Provider');
  }
  

	loadContacts(){
	let headers = new HttpHeaders({'Accept':'application/json','Authorization':this.authService.getToken()});
	this.httpClient.get('http://localhost:8000/api/contacts',{headers:headers}).subscribe(
	(response:any)=>{

		 this.contacts=response.contacts;
		 this.emitContactsSkills();
	},
    (error)=>{
    console.log("une erreur s'est produit :"+error);
    
    }
	);
	

	}

	addContact(contact:any){
	const formData=new FormData();
	formData.append('firstName',contact.firstName);
	formData.append('lastName',contact.lastName);
	formData.append('phone',contact.phone);
	contact.address?formData.append('address',contact.address):console.log("");
	contact.email?formData.append('email',contact.email):console.log("");
	contact.service?formData.append('service',contact.service):console.log("");
	let headers = new HttpHeaders({'Accept':'application/json','Authorization':this.authService.getToken()});
	this.httpClient.post('http://localhost:8000/api/contacts',formData,{headers:headers}).subscribe(
	(res)=>{
	this.loadContacts();
	let toastMessage=this.toastController.create({
  				message:"contact ajouté",
  				duration:2000
  				});
  	toastMessage.present();
	},
	(error)=>{
		if(error.status==400){
			let toastMessage=this.toastController.create({
  				message:"ce contact extiste déja",
  				duration:2000
  				});
  			toastMessage.present();
			
		}
		else{
		let toastMessage=this.toastController.create({
  				message:"une erreur s'est produit veuillez ressayer ulterieurment",
  				duration:2000
  				});
  			toastMessage.present();
	
		} 
	}
	);

	}
	updateCategorie(contact:any,id:any){
		const formData=new FormData();
		formData.append('id',id);
		contact.firstName?formData.append('firstName',contact.firstName):console.log("");
		contact.lastName?formData.append('lastName',contact.lastName):console.log("");
		contact.phone?formData.append('phone',contact.phone):console.log("");
		contact.address?formData.append('address',contact.address):console.log("");
		contact.email?formData.append('email',contact.email):console.log("");
		contact.service?formData.append('service',contact.service):console.log("");
		let headers = new HttpHeaders({'Accept':'application/json','Authorization':this.authService.getToken()});
		this.httpClient.post('http://localhost:8000/api/updatecontact',formData,{headers:headers}).subscribe(
		(res)=>{
			this.loadContacts();
			let toastMessage=this.toastController.create({
  				message:"contact modifié",
  				duration:2000
  				});
  			toastMessage.present();
			},
		(error)=>{
		
		if(error.status==500){
		let toastMessage=this.toastController.create({
  				message:error.error.updated,
  				duration:2000
  				});
  			toastMessage.present();
		
		}
		else{
		let toastMessage=this.toastController.create({
  				message:"une erreur s'est produit veuillez ressayer ulterieurment",
  				duration:2000
  				});
  			toastMessage.present();
		
		} 
		}
	);
	}

	deleteContact(id:number){
	let headers = new HttpHeaders({'Accept':'application/json','Authorization':this.authService.getToken()});
	this.httpClient.delete('http://localhost:8000/api/contacts?id='+id,{headers:headers}).subscribe(	
		(res:any)=>{
		this.loadContacts();
		
		},
		(error)=>{
		
		if(error.status==500 || error.status==501){
		let toastMessage=this.toastController.create({
  				message:error.error.deleted,
  				duration:2000
  				});
  			toastMessage.present();
		
		}
		else{
		let toastMessage=this.toastController.create({
  				message:"une erreur s'est produit veuillez ressayer ulterieurment",
  				duration:2000
  				});
  			toastMessage.present();
		
		} 
		}
	);
	
	}
	searchContacts(name:string){
		let arr=this.contacts.filter(w=>(w.firstName+w.lastName).includes(name));
		return arr;
	}
  

}
