import { Component, OnInit ,Inject} from '@angular/core';
import { FormGroup,FormBuilder,Validators} from '@angular/forms';
import { ContactService } from '../services/contactService.service';
import { MatDialogRef } from "@angular/material";
import {MAT_DIALOG_DATA} from '@angular/material';
@Component({
  selector: 'app-dialog-update-profile',
  templateUrl: './dialog-update-profile.component.html',
  styleUrls: ['./dialog-update-profile.component.css']
})
export class DialogUpdateProfileComponent implements OnInit {
  updateContactForm:FormGroup;
  constructor(private formBuilder:FormBuilder,private contactService:ContactService,private dialogRef:MatDialogRef<DialogUpdateProfileComponent>,@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  this.initForm();
  }
  initForm(){
  	this.updateContactForm=this.formBuilder.group(
  			{
  		   firstName:[''],
  		   lastName:[''],
  		   phone:['',[Validators.minLength(10)]],
  		   email:['',[Validators.email]],
  		   address:[''],
  		   service:['']

  		}
  			);
  }
  onSubmitForm(){
  		this.contactService.updateCategorie(this.updateContactForm.value,this.data);
  		this.dialogRef.close();
  }
  checkValidity(){
  	if( (this.updateContactForm.value["firstName"] || this.updateContactForm.value["lastName"] || this.updateContactForm.value["phone"] || this.updateContactForm.value["email"] || this.updateContactForm.value["address"] || this.updateContactForm.value["service"]) && this.updateContactForm.valid ){
  	return false;
  	}
  	else{
  		return true;
  	}
  }

}
