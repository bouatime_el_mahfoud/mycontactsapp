import { Component, OnInit ,Input} from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';
import { DialogUpdateProfileComponent } from '../dialog-update-profile/dialog-update-profile.component';
import { DialogDeleteProfileComponent } from '../dialog-delete-profile/dialog-delete-profile.component';
import { DialogViewProfileComponent } from '../dialog-view-profile/dialog-view-profile.component';

@Component({
  selector: 'app-view-contact',
  templateUrl: './view-contact.component.html',
  styleUrls: ['./view-contact.component.css']
})
export class ViewContactComponent implements OnInit {
  @Input() contact:any;
  constructor(private dialog:MatDialog) { }

  ngOnInit() {

  }
  onUpdate(){
  	const dialogRef=this.dialog.open(DialogUpdateProfileComponent,{data:this.contact.id});
  }
  onDelete(){
    const dialogRef=this.dialog.open(DialogDeleteProfileComponent,{data:this.contact.id});
  }
  onView(){
    const dialogRef=this.dialog.open(DialogViewProfileComponent,{data:this.contact});
  }
}
