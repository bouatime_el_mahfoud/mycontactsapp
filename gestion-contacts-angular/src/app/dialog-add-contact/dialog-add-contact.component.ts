import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators} from '@angular/forms';
import { ContactService } from '../services/contactService.service';
import { MatDialogRef } from "@angular/material";
@Component({
  selector: 'app-dialog-add-contact',
  templateUrl: './dialog-add-contact.component.html',
  styleUrls: ['./dialog-add-contact.component.css']
})

export class DialogAddContactComponent implements OnInit {
  addContactForm:FormGroup;
  constructor(private formBuilder:FormBuilder,private contactService:ContactService,private dialogRef:MatDialogRef<DialogAddContactComponent>) { }

  ngOnInit() {
  this.initForm();
  }
  initForm(){
  	this.addContactForm=this.formBuilder.group(
  			{
  		   firstName:['',Validators.required],
  		   lastName:['',Validators.required],
  		   phone:['',[Validators.required,Validators.minLength(10)]],
  		   email:['',[Validators.email]],
  		   address:[''],
  		   service:['']

  		}
  			);
  }
  onSubmitForm(){
  		this.contactService.addContact(this.addContactForm.value);
  		this.dialogRef.close();
  }
}
