import { Subject } from 'rxjs/Subject';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from './authService.service';
import { Location } from '@angular/common';
@Injectable()
export class ContactService{
	constructor(private httpClient:HttpClient,private authService:AuthService){
	
	}
	private contacts=[];
	contactsSubject=new Subject<any[]>();

	emitContactsSkills(){
		this.contactsSubject.next(this.contacts.slice());
	}

	loadContacts(){
	let headers = new HttpHeaders({'Accept':'application/json','Authorization':this.authService.getToken()});
	this.httpClient.get('http://localhost:8000/api/contacts',{headers:headers}).subscribe(
	(response:any)=>{
		this.contacts=response.contacts;
		this.emitContactsSkills();
	},
    (error)=>{
    console.log("une erreur s'est produit :"+error);
    }
	);
	}

	addContact(contact:any){
	const formData=new FormData();
	formData.append('firstName',contact.firstName);
	formData.append('lastName',contact.lastName);
	formData.append('phone',contact.phone);
	contact.address?formData.append('address',contact.address):console.log("");
	contact.email?formData.append('email',contact.email):console.log("");
	contact.service?formData.append('service',contact.service):console.log("");
	let headers = new HttpHeaders({'Accept':'application/json','Authorization':this.authService.getToken()});
	this.httpClient.post('http://localhost:8000/api/contacts',formData,{headers:headers}).subscribe(
	(res)=>{
	this.loadContacts();
	alert('contact ajouté')},
	(error)=>{
		if(error.status==400){
			alert('ce contact extiste déja');
		}
		else{
		alert("une erreur s'est produit veuillez ressayer ulterieurment");
		} 
	}
	);

	}
	updateCategorie(contact:any,id:any){
		const formData=new FormData();
		formData.append('id',id);
		contact.firstName?formData.append('firstName',contact.firstName):console.log("");
		contact.lastName?formData.append('lastName',contact.lastName):console.log("");
		contact.phone?formData.append('phone',contact.phone):console.log("");
		contact.address?formData.append('address',contact.address):console.log("");
		contact.email?formData.append('email',contact.email):console.log("");
		contact.service?formData.append('service',contact.service):console.log("");
		let headers = new HttpHeaders({'Accept':'application/json','Authorization':this.authService.getToken()});
		this.httpClient.post('http://localhost:8000/api/updatecontact',formData,{headers:headers}).subscribe(
		(res)=>{
			this.loadContacts();
			alert('contact modifié')},
		(error)=>{
		console.log(error);
		if(error.status==500){
		alert(error.error.updated);
		}
		else{
		alert("une erreur s'est produit veuillez ressayer ulterieurment");
		} 
		}
	);
	}

	deleteContact(id:number){
	let headers = new HttpHeaders({'Accept':'application/json','Authorization':this.authService.getToken()});
	this.httpClient.delete('http://localhost:8000/api/contacts?id='+id,{headers:headers}).subscribe(	
		(res:any)=>{
		this.loadContacts();
		
		},
		(error)=>{
		
		if(error.status==500 || error.status==501){
		alert(error.error.deleted);
		}
		else{
		alert("une erreur s'est produit veuillez ressayer ulterieurment");
		} 
		}
	);
	
	}

}