import { Injectable } from '@angular/core';
import { HttpClient ,HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { Subject } from 'rxjs/Subject';
@Injectable()
export class AuthService {
	storageKey:string="token";
    userSubject=new Subject<any>();
    private user:any;
	
	constructor(private httpClient:HttpClient,private router:Router){

	}
	setToken(token:string){
		localStorage.setItem(this.storageKey,token);
	}
	getToken(){
		return localStorage.getItem(this.storageKey);
	}
	
	logout(){
		localStorage.removeItem(this.storageKey);
		this.router.navigate(['/']);

	}
	login(email:string,password:string){
	this.httpClient.post('http://localhost:8000/api/login',{email:email,password:password}).subscribe(
	(res:any)=>{
		this.setToken('Bearer '+res.success.token);
		this.router.navigate(['/dashboard']);
		
	},
	err=>{
		if(err.status==401){
			alert('email ou mot de passe incorect');
		}
		else {
		alert("une erreur s'est produit ,veuillez essayer ulterieruement");
		}
	}

	);
	}
	signUp(user:any){
		
		this.httpClient.post('http://localhost:8000/api/signup',{email:user.email,password:user.password,firstName:user.firstName,lastName:user.lastName,phone:user.phone,address:user.address,service:user.service}).subscribe(
			(res:any)=>{
				this.setToken('Bearer '+res.success.token);
				this.router.navigate(['/dashboard']);
			},
			err=>{
			if(err.status==400){
			alert("cette address email est déja utilisé");
			}
			else {
				alert("une erreur s'est produit ,veuillez essayer ulterieruement");
			}
			
			}
		);
	}
	emitUserSubject(){
		this.userSubject.next(this.user);
	}
	loadUserInfos(){
		let headers = new HttpHeaders({'Accept':'application/json','Authorization':this.getToken()});
		this.httpClient.post('http://localhost:8000/api/infos',{},{headers:headers}).subscribe(
			(res:any)=>{
			
			this.user=res;
			this.emitUserSubject();
			},
			err=>{
				console.log(err);
			}
		);
	}
	logOut(){
		
		localStorage.removeItem(this.storageKey);
		this.router.navigate(['/']);

	}
	
	}