import { ActivatedRouteSnapshot,RouterStateSnapshot,CanActivate,Router } from '@angular/router';
import { Injectable } from '@angular/core'
import {Observable } from "rxjs/Observable";
import { AuthService } from './authService.service'
@Injectable()
export class AuthGuardService implements CanActivate{
	constructor(private authservice: AuthService,private router:Router){

	}
	canActivate(
	route:ActivatedRouteSnapshot,state:RouterStateSnapshot):Observable<boolean>| Promise<boolean> | boolean{
	if(this.authservice.getToken()){
	return true;
	}
	else{
		this.router.navigate(['/']);
	}

	}
}