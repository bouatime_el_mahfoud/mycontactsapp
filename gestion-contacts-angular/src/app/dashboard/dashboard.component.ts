import { Component, OnInit } from '@angular/core';
import {AuthService } from '../services/authService.service';
import {ContactService } from '../services/contactService.service';
import { MatDialog, MatDialogRef } from '@angular/material';
import { DialogAddContactComponent } from '../dialog-add-contact/dialog-add-contact.component';
import {Subscription } from 'rxjs/Subscription';
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  	user={};
  	contacts=[];
   	userSubscription:Subscription;
   	contactSubscription:Subscription;
  constructor(private authService:AuthService,private contactService:ContactService,private dialog:MatDialog) { }

  ngOnInit() {
  	 this.userSubscription=this.authService.userSubject.subscribe(
    (user:any)=>{
      
      this.user=user;
      
    }
    );
    this.contactSubscription=this.contactService.contactsSubject.subscribe(
    (contacts:any[])=>{
    	this.contacts=contacts;
    	console.log(contacts);
    }
    );
    this.authService.loadUserInfos();
    this.contactService.loadContacts();
  }

  onAddContact(){
  	const dialogRef=this.dialog.open(DialogAddContactComponent);
  }
  onLogOut(){
    this.authService.logOut();
  }

}
