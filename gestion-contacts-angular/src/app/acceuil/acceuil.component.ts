import { Component, OnInit } from '@angular/core';
import { FormGroup,FormBuilder,Validators} from '@angular/forms';
import { AuthService } from '../services/authService.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-acceuil',
  templateUrl: './acceuil.component.html',
  styleUrls: ['./acceuil.component.css']
})
export class AcceuilComponent implements OnInit {
  signUpForm:FormGroup;
  signInForm:FormGroup;
  state:number;
  constructor(private formBuilder:FormBuilder,private authService:AuthService,private router:Router) { }

  ngOnInit() {
  if(this.authService.getToken()){
    this.router.navigate(["/dashboard"]);
  }
  else{
    this.state=1;
    this.initForm();
  }
  
  }
  initForm(){
  	if(this.state===1){
  		this.signInForm=this.formBuilder.group(
  			{
  		email:['',[Validators.required,Validators.email]]
  		,password:['',[Validators.required,Validators.minLength(8)]]
  		}
  			);
  	}
  	else if(this.state===2){
  		this.signUpForm=this.formBuilder.group(
  			{
  		   firstName:['',Validators.required],
  		   lastName:['',Validators.required],
  		   phone:['',[Validators.required,Validators.minLength(10)]],
  		   email:['',[Validators.required,Validators.email]],
  		  password:['',[Validators.required,Validators.minLength(8)]],
  		  address:['',Validators.required],
  		  service:['',Validators.required]

  		}
  			);
  	}
  }
  onSwitch(){
  	if(this.state===1){
  	this.state=2;
  	this.initForm();

  	}
  	else if(this.state===2){
  		this.state=1;

  		this.initForm();
  	}
  }
  onSubmitForm(){
  	if(this.state===1){
 		this.authService.login(
    this.signInForm.value['email'],this.signInForm.value['password']
    );
 	}
 	else if(this.state===2){
 		this.authService.signUp(this.signUpForm.value);	
 	}
 	
  }

}
