import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogViewProfileComponent } from './dialog-view-profile.component';

describe('DialogViewProfileComponent', () => {
  let component: DialogViewProfileComponent;
  let fixture: ComponentFixture<DialogViewProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogViewProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogViewProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
