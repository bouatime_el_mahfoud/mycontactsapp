import { Component, OnInit , Inject } from '@angular/core';
import { MatDialogRef } from "@angular/material";
import { ContactService } from '../services/contactService.service';
import {MAT_DIALOG_DATA} from '@angular/material';
@Component({
  selector: 'app-dialog-delete-profile',
  templateUrl: './dialog-delete-profile.component.html',
  styleUrls: ['./dialog-delete-profile.component.css']
})
export class DialogDeleteProfileComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<DialogDeleteProfileComponent>,private contactService:ContactService,@Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
  }
  descision(result:boolean){
  if(result) this.contactService.deleteContact(this.data);
  this.dialogRef.close(result);
  }

}
