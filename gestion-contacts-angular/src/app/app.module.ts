import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material';
import { AcceuilComponent } from './acceuil/acceuil.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DialogViewProfileComponent } from './dialog-view-profile/dialog-view-profile.component';
import { DialogUpdateProfileComponent } from './dialog-update-profile/dialog-update-profile.component';
import { DialogDeleteProfileComponent } from './dialog-delete-profile/dialog-delete-profile.component';
import { ViewContactComponent } from './view-contact/view-contact.component';
import { HttpClientModule } from '@angular/common/http';
import { AuthService } from './services/authService.service';
import { ContactService } from './services/contactService.service';
import { AuthGuardService } from './services/authGuardService.service';
import { DialogAddContactComponent } from './dialog-add-contact/dialog-add-contact.component';

@NgModule({
  declarations: [
    AppComponent,
    AcceuilComponent,
    DashboardComponent,
    DialogViewProfileComponent,
    DialogUpdateProfileComponent,
    DialogDeleteProfileComponent,
    ViewContactComponent,
    DialogAddContactComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatDialogModule
  ],
  providers: [AuthService,ContactService,AuthGuardService],
  bootstrap: [AppComponent],
  entryComponents:[DialogAddContactComponent,DialogViewProfileComponent,DialogUpdateProfileComponent,DialogDeleteProfileComponent]
})
export class AppModule { }
